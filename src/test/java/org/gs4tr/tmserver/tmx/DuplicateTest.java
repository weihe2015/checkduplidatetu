package org.gs4tr.tmserver.tmx;

import java.util.HashMap;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

public class DuplicateTest
{
	@Test
	public void duplicateTest1() throws Exception
	{
		Map<String, Integer> map = new HashMap<>();
		map.put("This is a test.", 1);
		Assert.assertTrue(map.containsKey("This is a test."));
	} 
}
