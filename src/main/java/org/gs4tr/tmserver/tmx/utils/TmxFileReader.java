package org.gs4tr.tmserver.tmx.utils;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.ElementHandler;
import org.dom4j.ElementPath;
import org.dom4j.io.SAXReader;
import org.gs4tr.foundation.locale.Locale;
import org.gs4tr.tmserver.tmx.common.MonitoringFileInputStream;
import org.gs4tr.tmserver.tmx.common.TmxConstants;
import org.gs4tr.tmserver.tmx.common.TmxDtdResolver;
import org.gs4tr.tmserver.tmx.common.TmxHeader;
import org.gs4tr.tmserver.tmx.common.TmxReaderCallback;
import org.gs4tr.tmserver.tmx.common.TmxReaderCallbackImpl;
import org.gs4tr.tmserver.tmx.common.Tu;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class TmxFileReader implements TmxConstants
{
	public class TmxHeaderElementHandler implements ElementHandler
	{
		@Override
		public void onEnd(ElementPath path)
		{
			Element element = path.getCurrent();

			m_header = new TmxHeader(element);
			m_header.setTmxVersion(m_tmxVersion);

			if (m_callback != null)
			{
				m_callback.handle(m_header);
			}
			// prune the current element to reduce memory
			element.detach();
		}

		@Override
		public void onStart(ElementPath path)
		{
		}
	}

	private String m_tmxVersion;
	private String m_fileName;
	private Tu m_tu;
	private TmxHeader m_header;
	private TmxReaderCallback m_callback;
	private Locale sourceLocale;
	private Locale targetLocale;
	
	public String getFileName()
	{
		return m_fileName;
	}

	public TmxHeader getHeader()
	{
		return m_header;
	}

	public String getTmxVersion()
	{
		return m_tmxVersion;
	}

	@SuppressWarnings("unused")
	private void parseFile(SAXReader p_reader, InputSource p_input,
			MonitoringFileInputStream p_stream) throws DocumentException
	{
		SAXReader reader = p_reader;
		final MonitoringFileInputStream stream = p_stream;
		Map<Tu, Integer> map = new HashMap<>();
		// enable element complete notifications to conserve memory
		reader.addHandler("/" + TmxConstants.ROOT + "/" + TmxConstants.HEADER,
				new TmxHeaderElementHandler());
		// enable element complete notifications to conserve memory
		reader.addHandler("/" + TmxConstants.ROOT + "/" + TmxConstants.BODY + "/" + TmxConstants.TU,
				new ElementHandler() {
					int tuCount = 0;
					
					@Override
					final public void onEnd(ElementPath path)
					{
						shout();
						Element element = path.getCurrent();

						if (m_callback != null)
						{
							m_tu = new Tu(element);
							m_callback.handle(m_tu);
						}
						// prune the current element to reduce memory
						element.detach();
					}

					@Override
					final public void onStart(ElementPath path)
					{
					}

					final private void shout()
					{
						tuCount++;
						int prec = (int) (stream.getCounter() * 100 / stream.getTotal());
						System.err.println("FileName: " + FilenameUtils.getBaseName(m_fileName) + ", process: " + prec);
					}
				});
		reader.read(p_input);
	}

	public void readFile(String p_filename)
			throws SAXException, FileNotFoundException, DocumentException
	{
		m_fileName = p_filename;
		m_callback = new TmxReaderCallbackImpl();
		m_callback.setLocales(sourceLocale, targetLocale);
		
		SAXReader reader = new SAXReader();
		reader.setFeature("http://xml.org/sax/features/external-general-entities", false);
		reader.setFeature("http://xml.org/sax/features/external-parameter-entities", false);
		reader.setFeature("http://apache.org/xml/features/validation/dynamic", false);
		reader.setEntityResolver(new TmxDtdResolver());
		reader.setValidation(false);

		MonitoringFileInputStream stream = new MonitoringFileInputStream(p_filename);
		InputSource src = new InputSource(stream);
		parseFile(reader, src, stream);
		
		
	}
	
	public void setLocales(Locale sourceLocale, Locale targetLocale)
	{
		this.sourceLocale = sourceLocale;
		this.targetLocale = targetLocale;
	}
	
	public List<Tu> getList()
	{
		return m_callback.getList();
	}
}
