package org.gs4tr.tmserver.tmx.common;

import java.util.List;

import org.gs4tr.foundation.locale.Locale;

public interface TmxReaderCallback
{
	void handle(TmxHeader header);

	boolean handle(Tu tu);
	
	void setLocales(Locale sourceLocale, Locale targetLocale);
	
	List<Tu> getList();
}
