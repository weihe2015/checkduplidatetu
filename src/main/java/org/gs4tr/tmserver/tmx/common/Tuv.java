package org.gs4tr.tmserver.tmx.common;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.dom4j.Element;
import org.gs4tr.foundation3.core.utils.UTC;
import org.gs4tr.foundation3.xml.XmlParser;

public class Tuv implements TmxConstants
{
	static private transient String EOL = System.getProperty("line.separator");

	private String m_lang;
	private Element m_segment;

	private String m_datatype;
	private String m_creationdate;
	private String m_creationid;
	private String m_changedate;
	private String m_changeid;
	private String m_usagecount;
	private String m_lastusagedate;
	private String m_creationtool;
	private String m_creationtoolversion;
	private String m_o_tmf;
	private String m_o_encoding;

	private List<Element> m_props = new ArrayList<Element>();

	public Tuv()
	{
	}

	public Tuv(Element p_root)
	{
		init(p_root);
	}

	public Tuv(String p_language)
	{
		m_lang = p_language;
	}

	public void addProp(String p_xml)
	{
		// TODO: using the root element undetached from the document keeps the document
		// in memory.
		// Not sure if that's a huge overhead but consider using
		// ...getRootElement().detach();
		Element prop = XmlParser.parseXml(p_xml).getRootElement();
		m_props.add(prop);
	}

	public void setProps(List<Element> p_props)
	{
		m_props = p_props;
	}
	
	public void clearProps()
	{
		m_props.clear();
	}

	public String getChangeDate()
	{
		return m_changedate;
	}

	public Date getChangeDateAsDate()
	{
		return StringUtils.isNotBlank(m_changedate) ? UTC.parseNoSeparators(m_changedate) : null;
	}

	public String getChangeId()
	{
		return m_changeid;
	}

	public String getCreationDate()
	{
		return m_creationdate;
	}

	public Date getCreationDateAsDate()
	{
		return StringUtils.isNotBlank(m_creationdate) ? UTC.parseNoSeparators(m_creationdate)
				: null;
	}

	public String getCreationId()
	{
		return m_creationid;
	}

	public String getDataType()
	{
		return m_datatype;
	}

	public String getLanguage()
	{
		return m_lang;
	}

	public List<Element> getProps()
	{
		return m_props;
	}

	public Element getSegment()
	{
		return m_segment;
	}

	public String getText()
	{
		return m_segment.getText();
	}

	public String getUsageCount()
	{
		return m_usagecount;
	}

	public String getXml()
	{
		return getXml(true);
	}

	public String getXml(boolean writeXmlLang)
	{
		StringBuffer result = new StringBuffer(128);

		result.append("    <tuv ");
		result.append(writeXmlLang ? "xml:lang=\"" : "lang=\"");
		result.append(EditUtil.encodeXmlEntities(m_lang));
		result.append("\"");

		if (StringUtils.isNotBlank(m_datatype))
		{
			result.append(" datatype=\"");
			result.append(EditUtil.encodeXmlEntities(m_datatype));
			result.append("\"");
		}
		if (StringUtils.isNotBlank(m_creationdate))
		{
			result.append(" creationdate=\"");
			result.append(EditUtil.encodeXmlEntities(m_creationdate));
			result.append("\"");
		}
		if (StringUtils.isNotBlank(m_creationid))
		{
			result.append(" creationid=\"");
			result.append(EditUtil.encodeXmlEntities(m_creationid));
			result.append("\"");
		}
		if (StringUtils.isNotBlank(m_changedate))
		{
			result.append(" changedate=\"");
			result.append(EditUtil.encodeXmlEntities(m_changedate));
			result.append("\"");
		}
		if (StringUtils.isNotBlank(m_changeid))
		{
			result.append(" changeid=\"");
			result.append(EditUtil.encodeXmlEntities(m_changeid));
			result.append("\"");
		}
		if (StringUtils.isNotBlank(m_usagecount))
		{
			result.append(" usagecount=\"");
			result.append(EditUtil.encodeXmlEntities(m_usagecount));
			result.append("\"");
		}
		if (StringUtils.isNotBlank(m_lastusagedate))
		{
			result.append(" lastusagedate=\"");
			result.append(EditUtil.encodeXmlEntities(m_lastusagedate));
			result.append("\"");
		}
		if (StringUtils.isNotBlank(m_creationtool))
		{
			result.append(" creationtool=\"");
			result.append(EditUtil.encodeXmlEntities(m_creationtool));
			result.append("\"");
		}
		if (StringUtils.isNotBlank(m_creationtoolversion))
		{
			result.append(" creationtoolversion=\"");
			result.append(EditUtil.encodeXmlEntities(m_creationtoolversion));
			result.append("\"");
		}
		if (StringUtils.isNotBlank(m_o_tmf))
		{
			result.append(" o-tmf=\"");
			result.append(EditUtil.encodeXmlEntities(m_o_tmf));
			result.append("\"");
		}
		if (StringUtils.isNotBlank(m_o_encoding))
		{
			result.append(" o-encoding=\"");
			result.append(EditUtil.encodeXmlEntities(m_o_encoding));
			result.append("\"");
		}

		result.append(">");
		result.append(EOL);

		for (int i = 0, max = m_props.size(); i < max; ++i)
		{
			Element prop = m_props.get(i);
			result.append("      ");
			result.append(prop.asXML());
			result.append(EOL);
		}

		result.append("      ");
		result.append(m_segment.asXML());
		result.append(EOL);
		result.append("    </tuv>");
		result.append(EOL);

		return result.toString();
	}

	@SuppressWarnings("unchecked")
	private void init(Element p_root)
	{
		clearProps();

		m_lang = p_root.attributeValue(LANG);
		m_segment = p_root.element(SEG);

		m_datatype = p_root.attributeValue(DATATYPE);
		m_creationdate = p_root.attributeValue(CREATIONDATE);
		m_creationid = p_root.attributeValue(CREATIONID);
		m_changedate = p_root.attributeValue(CHANGEDATE);
		m_changeid = p_root.attributeValue(CHANGEID);
		m_usagecount = p_root.attributeValue(USAGECOUNT);
		m_lastusagedate = p_root.attributeValue(LASTUSAGEDATE);
		m_creationtool = p_root.attributeValue(CREATIONTOOL);
		m_creationtoolversion = p_root.attributeValue(CREATIONTOOLVERSION);
		m_o_tmf = p_root.attributeValue(O_TMF);
		m_o_encoding = p_root.attributeValue(O_ENCODING);

		for (Iterator<Element> it = p_root.elementIterator(PROP); it.hasNext();)
		{
			Element item = it.next();
			m_props.add(item);
		}
	}

	public void setChangeDate(Date changedate)
	{
		this.m_changedate = UTC.valueOfNoSeparators(changedate);
	}

	public void setChangeDate(String changedate)
	{
		this.m_changedate = changedate;
	}

	public void setChangeId(String changeid)
	{
		this.m_changeid = changeid;
	}

	public void setCreationDate(Date creationdate)
	{
		this.m_creationdate = UTC.valueOfNoSeparators(creationdate);
	}

	public void setCreationDate(String creationdate)
	{
		this.m_creationdate = creationdate;
	}

	public void setCreationId(String creationid)
	{
		this.m_creationid = creationid;
	}

	public void setDataType(String type)
	{
		m_datatype = type;
	}

	public void setLanguage(String lang)
	{
		m_lang = lang;
	}

	/**
	 * Sets the segment to a DOM Element whose root element must be &lt;seg&gt;.
	 */
	public void setSegment(Element p_segment)
	{
		m_segment = p_segment;
	}

	/**
	 * Sets the content to an XML string whose root element must be "&lt;seg&gt;".
	 */
	public void setSegment(String p_xml)
	{
		// TODO: using the root element undetached from the document keeps the document
		// in memory.
		// Not sure if that's a huge overhead but consider using
		// ...getRootElement().detach();
		m_segment = XmlParser.parseXml(p_xml).getRootElement();
	}

	public void setText(String p_text)
	{
		m_segment.setText(p_text);
	}

	public void setUsageCount(String usagecount)
	{
		this.m_usagecount = usagecount;
	}
}
