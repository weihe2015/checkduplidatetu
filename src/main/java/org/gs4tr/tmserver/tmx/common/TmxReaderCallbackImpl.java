package org.gs4tr.tmserver.tmx.common;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.gs4tr.foundation.locale.Locale;

public class TmxReaderCallbackImpl implements TmxReaderCallback
{
	private String sourceLanguage;
	
	private Locale sourceLocale;
	private Locale targetLocale;
	
	private List<Tu> list = new ArrayList<>();
	
	@Override
	public void handle(TmxHeader header)
	{
		sourceLanguage = header.getSourceLang();
	}

	public void setLocales(Locale sourceLocale, Locale targetLocale)
	{
		this.sourceLocale = sourceLocale;
		this.targetLocale = targetLocale;
	}
	
	@Override
	public boolean handle(Tu tu)
	{
		if (tu.getTuvs().size() != 2)
		{
			return false;
		}

		Tuv sourceTuv = tu.getTuvs().get(0);
		Tuv targetTuv = tu.getTuvs().get(1);
		
		if (StringUtils.equals(sourceLanguage, sourceTuv.getLanguage()) 
				&& StringUtils.equalsIgnoreCase(sourceLocale.getCode(), sourceTuv.getLanguage()))
		{
			sourceTuv.setProps(tu.getProps());
		}
		else if (StringUtils.equalsIgnoreCase(targetLocale.getCode(), targetTuv.getLanguage()))
		{
			targetTuv.setProps(tu.getProps());
		}
		else
		{
			System.err.println("Locale does not match. file locale: " + 
					sourceTuv.getLanguage() + " provided Locale: " + sourceLocale.getLanguage());
			return false;
		}
		list.add(tu);
		return true;
	}
	
	@Override
	public List<Tu> getList()
	{
		return this.list;
	}
}
