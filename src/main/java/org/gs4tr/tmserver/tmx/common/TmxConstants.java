package org.gs4tr.tmserver.tmx.common;

/**
 * This class provides element and attribute names for the TMX DTD in proper
 * spelling and case, as well as version and DTD constants.
 *
 * @see http://www.lisa.org/fileadmin/standards/tmx1.4/tmx.htm
 */
public interface TmxConstants
{
	//
	// TMX version constants
	//

	static public final String TMX_11 = "1.1";
	static public final String TMX_12 = "1.2";
	static public final String TMX_13 = "1.3";
	static public final String TMX_14 = "1.4";
	static public final String TMX_DTD_11 = "tmx11.dtd";
	static public final String TMX_DTD_12 = "tmx12.dtd";
	static public final String TMX_DTD_13 = "tmx13.dtd";
	static public final String TMX_DTD_14 = "tmx14.dtd";
	static public final int TMX_VERSION_11 = 110;
	static public final int TMX_VERSION_12 = 120;
	static public final int TMX_VERSION_13 = 130;
	static public final int TMX_VERSION_14 = 140;

	// Native format of this implementation.
	static public final String TMX_NATIVE = "1.0 TDC";
	static public final String TMX_DTD_NATIVE = "tmx-tdc.dtd";
	static public final int TMX_VERSION_NATIVE = 142;

	//
	// Constants for TMX element names.
	//
	static final public String ROOT = "tmx";
	static final public String HEADER = "header";
	static final public String MAP = "map";
	static final public String UDE = "ude";
	static final public String BODY = "body";
	static final public String TU = "tu";
	static final public String TUV = "tuv";
	static final public String SEG = "seg";
	static final public String PROP = "prop";
	static final public String NOTE = "note";

	// Constants for segment-internal markup
	static final public String BPT = "bpt";
	static final public String EPT = "ept";
	static final public String PH = "ph";
	static final public String IT = "it";
	static final public String UT = "ut";

	// HI appears anywhere, SUB only within internal tags.
	static final public String HI = "hi";
	static final public String SUB = "sub";

	//
	// Constants for TMX attribute names.
	//
	static final public String ADMINLANG = "adminlang";
	static final public String CHANGEDATE = "changedate";
	static final public String CHANGEID = "changeid";
	static final public String CREATIONDATE = "creationdate";
	static final public String CREATIONID = "creationid";
	static final public String CREATIONTOOL = "creationtool";
	static final public String CREATIONTOOLVERSION = "creationtoolversion";
	static final public String DATATYPE = "datatype";
	static final public String LASTUSAGEDATE = "lastusagedate";
	static final public String O_ENCODING = "o-encoding";
	static final public String O_TMF = "o-tmf";
	static final public String SEGTYPE = "segtype";
	static final public String SRCLANG = "srclang";
	static final public String TUID = "tuid";
	static final public String TYPE = "type";
	static final public String USAGECOUNT = "usagecount";
	static final public String VERSION = "version";
	static final public String X = "x";
	static final public String I = "i";

	static final public String POS = "pos";
	static final public String POS_BEGIN = "begin";
	static final public String POS_END = "end";

	static final public String ASSOC = "assoc";
	static final public String ASSOC_PRECEDING = "p";
	static final public String ASSOC_FOLLOWING = "f";
	static final public String ASSOC_BOTH = "b";

	// careful: must write "xml:lang" but read "lang"
	static final public String LANG = "lang";

	//
	// User-defined constants we use in <prop> when writing out GXML.
	//
	static final public String PROP_SEGMENTTYPE = "tdc-segment-type";

	static final public String PROP_TUTYPE = "tdc-tu-type";
	static final public String VAL_TU_LOCALIZABLE = "localizable";
	static final public String VAL_TU_TRANSLATABLE = "translatable";

	//
	// System-defined constants when writing out the TMX header
	//
	static final public String APPNAME = "TDC TM Server";
	static final public String APPVERSION = "1.0";

	// segtype values
	static final public String SEGMENTATION_BLOCK = "block";
	static final public String SEGMENTATION_PARAGRAPH = "paragraph";
	static final public String SEGMENTATION_SENTENCE = "sentence";
	static final public String SEGMENTATION_PHRASE = "phrase";

	// o-tmf values: our pivot is TXML, so this is our original TM format
	static final public String TMF_TXML = "txml";

	// adminlang values: en_US by default
	static final public String DEFAULT_ADMINLANG = "EN-US";

	// datatype values: pivot is HTML, so all segments are HTML by default
	static final public String DATATYPE_HTML = "html";

	// srclang values: en_US by default
	static final public String DEFAULT_SOURCELANG = "EN-US";

	// default user that created a TU/TUV if information is missing.
	static final public String DEFAULT_USER = "system";
}