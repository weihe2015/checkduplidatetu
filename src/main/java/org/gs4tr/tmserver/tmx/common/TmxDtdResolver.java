package org.gs4tr.tmserver.tmx.common;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.xml.sax.EntityResolver;
import org.xml.sax.InputSource;

/**
 * An implementation of EntityResolver that finds various TMX DTDs in the
 * application's /properties directory.
 */
public class TmxDtdResolver implements EntityResolver
{
	static private final Pattern RE_DTD = Pattern.compile("^.*(tmx.*?\\.dtd)\\s*$");

	public TmxDtdResolver()
	{
	}

	@Override
	public InputSource resolveEntity(String publicId, String systemId)
	// throws org.xml.sax.SAXException
	{
		Matcher matcher = RE_DTD.matcher(systemId);

		if (matcher.find())
		{
			InputStream stream = getClass().getClassLoader()
					.getResourceAsStream("properties/tmx/" + matcher.group(1));

			if (stream != null)
			{
				return new InputSource(stream);
			}
		}
		return new InputSource(new ByteArrayInputStream(new byte[0]));
	}
}
