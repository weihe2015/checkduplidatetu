package org.gs4tr.tmserver.tmx.common;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class MonitoringFileInputStream extends FileInputStream
{
	long _counter = 0L;
	long _total = 0L;

	public MonitoringFileInputStream(File file) throws FileNotFoundException
	{
		super(file);

		_total = file.length();
	}

	public MonitoringFileInputStream(String file) throws FileNotFoundException
	{
		super(file);
		_total = new File(file).length();
	}

	public long getCounter()
	{
		return _counter;
	}

	public long getTotal()
	{
		return _total;
	}

	@Override
	public int read() throws IOException
	{
		final int i = super.read();
		if (i != -1)
		{
			_counter++;
		}
		return i;
	}

	@Override
	public int read(byte[] b) throws IOException
	{
		final int i = super.read(b);
		if (i > 0)
		{
			_counter += i;
		}
		return i;
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException
	{
		final int i = super.read(b, off, len);
		if (i > 0)
		{
			_counter += i;
		}
		return i;
	}

	@Override
	public void reset() throws IOException
	{
		super.reset();
		_counter = 0;
	}

	@Override
	public long skip(long n) throws IOException
	{
		final long i = super.skip(n);
		if (i > 0)
		{
			_counter += i;
		}
		return i;
	}

}
