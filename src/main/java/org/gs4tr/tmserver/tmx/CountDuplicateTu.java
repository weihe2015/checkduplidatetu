package org.gs4tr.tmserver.tmx;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.configuration.BaseConfiguration;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.gs4tr.foundation.locale.Locale;
import org.gs4tr.foundation3.core.commandline.Arguments;
import org.gs4tr.foundation3.io.utils.DirectoryUtils;
import org.gs4tr.tmserver.tmx.common.Tu;
import org.gs4tr.tmserver.tmx.common.Tuv;
import org.gs4tr.tmserver.tmx.utils.TmxFileReader;
import org.xml.sax.SAXException;


public class CountDuplicateTu {

	public static void main(String[] args) 
	{
		System.exit(main2(args));
	}

	static public int main2(String argv[])
	{
		List<String> fileNames;
		Locale sourceLocale = Locale.US;
		Locale targetLocale = Locale.CHINA;
		String reportFile = null;
		boolean checkTargetTextAsDuplicate = false;
		Configuration config = new BaseConfiguration();

		try
		{
			Arguments getopt = new Arguments();
			String temp;
			int c;
			
			getopt.setUsage(new String[] {
				"Usage: CountDuplidateTu -c config [-r reportFile] file/folder ...",
				"",
				"Count duplidate Tus in TMX files. A report is written to stdout",
				"",
				"Options:",
				"\t-c config: configuration file, required",
				"\t-r report File: specifies report file which includes report in it, optional",
				"\t-h help: show this help"
			});
			getopt.parseArgumentTokens(argv, new char[] { 'c', 'r'});
			
			while ((c = getopt.getArguments()) != -1)
			{
				switch (c)
				{
				case 'c':
					temp = getopt.getStringParameter(); 
                    if (!new File(temp).exists())
                    {
                        System.err.println("Config file `" + temp + "' does not exist.\n");
                        getopt.printUsage();
                        return -1;
                    }
                    config = new PropertiesConfiguration(temp);
					break;
                case 'r':
                    reportFile = getopt.getStringParameter();
                    break;
                case 'h':
                default:
                    if (c != 'h') System.err.println("Unrecognized option -" + (char)c + ".\n");
                    getopt.printUsage();
                    return 1;
				}
			}
			
			fileNames = getopt.getAllFiles();
            fileNames = DirectoryUtils.enumerateFiles(fileNames,
                    new String[]{ "tmx" });
            if (fileNames.size() == 0)
            {
                System.err.println("No Tmx files found.\n");
                getopt.printUsage();
                return 1;
            }
            
            System.err.println("Counting Duplidate TUs in " + fileNames.size() + " tmx file" +
                    (fileNames.size() == 1 ? "" : "s") + ".");
            
            // get source and target locales from config file.
            if (!config.containsKey("sourceLocale"))
            {
            	System.err.println("Missing source Locale in the config file.");
            	getopt.printUsage();
            	return 1;
            }
            if (!config.containsKey("targetLocale"))
            {
            	System.err.println("Missing target Locale in the config file.");
            	getopt.printUsage();
            	return 1;
            }
            
            temp = config.getString("sourceLocale");
            if (Locale.checkLocale(temp)) 
            {
                sourceLocale = Locale.makeLocale(temp);
            } else 
            {
                System.err.println("Invalid source locale -l " + temp //$NON-NLS-1$
                        + ", specify as XX-YY.\n"); //$NON-NLS-1$
                getopt.printUsage();
                return -1;
            }
            
            temp = config.getString("targetLocale");
            if (Locale.checkLocale(temp)) 
            {
            	targetLocale = Locale.makeLocale(temp);
            } else 
            {
                System.err.println("Invalid target locale -l " + temp //$NON-NLS-1$
                        + ", specify as XX-YY.\n"); //$NON-NLS-1$
                getopt.printUsage();
                return -1;
            }
            
            // Add Target Text as duplicate checking:
            if (config.containsKey("includeTargetTextAsDuplicate"))
            {
            	checkTargetTextAsDuplicate = config.getBoolean("includeTargetTextAsDuplicate");
            }
            
            CountDuplicateTu countDuplidateTu = new CountDuplicateTu();
            countDuplidateTu.setFileNames(fileNames);
            countDuplidateTu.setLocales(sourceLocale, targetLocale);
            countDuplidateTu.setConfiguration(config);
            countDuplidateTu.setReportFile(reportFile);
            countDuplidateTu.setCheckTargetTextAsDuplicate(checkTargetTextAsDuplicate);
            countDuplidateTu.countDuplidates();
		}
		catch (Throwable ex)
		{
			ex.printStackTrace();
			return 1;
		}
		
		return 0;
	}
	
	private List<String> fileNames;
	private Locale sourceLocale;
	private Locale targetLocale;
	private Configuration config;
	private String reportFile;
	private boolean checkTargetTextAsDuplicate;
	
	private void setFileNames(List<String> fileNames)
	{
		this.fileNames = fileNames;
	}
	
	private void setLocales(Locale sourceLocale, Locale targetLocale)
	{
		this.sourceLocale = sourceLocale;
		this.targetLocale = targetLocale;
	}
	
	private void setConfiguration(Configuration config)
	{
		this.config = config;
	}
	
	public void setReportFile(String reportFile)
	{
		this.reportFile = reportFile;
	}
	
	public void setCheckTargetTextAsDuplicate(boolean checkTargetTextAsDuplicate)
	{
		this.checkTargetTextAsDuplicate = checkTargetTextAsDuplicate;
	}
	
	private void countDuplidates()
	{
		List<Tu> totalList = new ArrayList<>();
		
		for (String fileName : fileNames)
		{
			TmxFileReader reader = new TmxFileReader();
			reader.setLocales(sourceLocale, targetLocale);
			try
			{
				reader.readFile(fileName);
				List<Tu> list = reader.getList();
				totalList.addAll(list);
			}
			catch (FileNotFoundException | SAXException | DocumentException e)
			{
				System.err.println("Exception with reading Tmx file: " + fileName + " " + e.getMessage());
			}
		}
		String attributes = null;
		Set<String> set = new HashSet<>();
		if (config.containsKey("attributes"))
		{
			attributes = config.getString("attributes");
			String[] attributeList = attributes.split(",");
			for (String attribute : attributeList)
			{
				set.add(attribute.trim());
			}
		}
		
		Map<String, Integer> map = new HashMap<>();
		
		if (!totalList.isEmpty())
		{
			for (Tu tu : totalList)
			{
				Tuv sourceTuv = tu.getTuvs().get(0);
				Tuv targetTuv = tu.getTuvs().get(1);
				
				if (StringUtils.equalsIgnoreCase(sourceLocale.getCode(), targetTuv.getLanguage()))
				{
					sourceTuv = tu.getTuvs().get(1);
					targetTuv = tu.getTuvs().get(0);
				}
				
				String text = sourceTuv.getSegment().getText();
				if (!StringUtils.equalsIgnoreCase(sourceLocale.getCode(), sourceTuv.getLanguage()))
				{
					System.err.println("Tu with text: " + text + " has souce Locale mismatch. ");
					System.err.println("Provided: " + sourceLocale.getCode() + " Tu: " + sourceTuv.getLanguage());
					continue;
				}
				else if (!StringUtils.equalsIgnoreCase(targetLocale.getCode(), targetTuv.getLanguage()))
				{
					System.err.println("Tu with text: " + text + " has target Locale mismatch. ");
					System.err.println("Provided: " + targetLocale.getCode() + " Tu: " + targetTuv.getLanguage());
					continue;
				}
				
				if (set.size() > 0)
				{
					StringBuffer sb = new StringBuffer();
					sb.append(text);
					if (checkTargetTextAsDuplicate)
					{
						sb.append(targetTuv.getSegment().getText());
					}
					List<Element> props = tu.getProps();
					for (String attribute : set)
					{
						for (Element ele : props)
						{
							String attributeName = ele.attributeValue("type");
							if (StringUtils.equalsIgnoreCase(attribute, attributeName))
							{
								String attributeValue = ele.getText();
								sb.append(attributeValue);
							}
						}
					}
					text = sb.toString();
				}
				else
				{
					StringBuffer sb = new StringBuffer();
					sb.append(text);
					if (checkTargetTextAsDuplicate)
					{
						sb.append(targetTuv.getSegment().getText());
					}
					text = sb.toString();
				}
				
				if (map.containsKey(text))
				{
					int num = map.get(text);
					num++;
					map.put(text, num);
				}
				else
				{
					map.put(text, 1);
				}
			}
			
			int duplicateCount = 0;
			for (String sourceText : map.keySet())
			{
				int num = map.get(sourceText);
				if (num > 1)
				{
					duplicateCount++;
				}
			}
			float overlapRate = (float) duplicateCount * 100 / (float) totalList.size();
			if (StringUtils.isNotEmpty(reportFile))
			{
				PrintWriter writer = null;
				try
				{
					File file = new File(reportFile);
					if (!file.exists())
					{
						file.createNewFile();
					}
					writer = new PrintWriter(new OutputStreamWriter(new FileOutputStream(reportFile), "UTF-8"));
					writer.print("Combining all " + fileNames.size() + " TMX files shown ");
					writer.printf("%.2f", overlapRate);
					writer.print("% overlapped TUs\n");
					writer.println("That is total of " + totalList.size() + " Tus, " + "duplicates: " + duplicateCount + ".");
					writer.flush();
				}
				catch (UnsupportedEncodingException | FileNotFoundException e)
				{
					System.err.println(e.getMessage());
				}
				catch (IOException e)
				{
					System.err.println(e.getMessage());
				}
				finally
				{
					if (writer != null)
					{
						writer.close();
					}
				}
				
			}
			else
			{
				System.out.print("Combining all " + fileNames.size() + " TMX files shown ");
				System.out.printf("%.2f", overlapRate);
				System.out.print("% overlapped TUs\n");
				System.out.println("That is total of " + totalList.size() + " Tus, " + "duplicates: " + duplicateCount + ".");
			}
		}
	}
}
