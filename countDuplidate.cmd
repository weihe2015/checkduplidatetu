@echo off

rem Slurp the command line arguments. This loop allows for an unlimited number
rem of arguments (up to the command line limit, anyway).
set CMD_LINE_ARGS=%1
if ""%1""=="""" goto doneStart
shift
:setupArgs
if ""%1""=="""" goto doneStart
set CMD_LINE_ARGS=%CMD_LINE_ARGS% %1
shift
goto setupArgs

:doneStart

set _JAVACMD=%JAVACMD%

if "%JAVA_HOME%" == "" goto noJavaHome
if not exist "%JAVA_HOME%\bin\java.exe" goto noJavaHome
if "%_JAVACMD%" == "" set _JAVACMD=%JAVA_HOME%\bin\java.exe

:noJavaHome
if "%_JAVACMD%" == "" set _JAVACMD=java.exe

set JAVA_OPTS=-Xmx4g -Djsse.enableSNIExtension=false %JAVA_OPTS%

"%_JAVACMD%" %JAVA_OPTS% -jar -Dlog4j.configuration=file:log4j.properties gs4tr-tm3-tmx-0.0.1-SNAPSHOT.jar %CMD_LINE_ARGS%